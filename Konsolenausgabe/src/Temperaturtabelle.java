
public class Temperaturtabelle {
	
	public static void main(String[] args) {
		
		//Strings
		String FahrenheitTitel = "Fahrenheit";
		String CelsiusTitel = "Celsius";
		String Horizontal = "-";
		String Vertikal = "|";
		String Plus = "+";
		String leer = "";
		
		//integer
		int FahrenheitA = -20;
		int FahrenheitB = -10;
		int FahrenheitC = +0;
		int FahrenheitD = +20;
		int FahrenheitE = +30;
		
		//Double
		Double CelsiusA = -28.89; //%.2f
		Double CelsiusB = -23.33;
		Double CelsiusC = -17.78;
		Double CelsiusD = -6.67;
		Double CelsiusE = -1.11;
		
		//Text
		System.out.printf( "%s%-2s%s%s%s %s %s %s%4s\n" ,     FahrenheitTitel, leer, Vertikal, leer, leer, leer, leer, CelsiusTitel, leer);
		System.out.printf( "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n" ,     Horizontal, Horizontal, Horizontal, Horizontal, Horizontal, Horizontal, Horizontal, Horizontal, Horizontal, Horizontal, Horizontal, Horizontal, Horizontal, Horizontal, Horizontal, Horizontal, Horizontal, Horizontal, Horizontal, Horizontal, Horizontal, Horizontal, Horizontal, Horizontal, Horizontal);
		System.out.printf( "%s%-3s%s%s%s %s %s %s %s %s %s %s %s %s %s%4s\n" ,     FahrenheitA, leer, leer, leer, leer, leer, leer, leer, leer, leer, Vertikal, leer, leer, leer, CelsiusA, leer);
		System.out.printf( "%s%-3s%s%s%s %s %s %s %s %s %s %s %s %s %s%4s\n" ,     FahrenheitB, leer, leer, leer, leer, leer, leer, leer, leer, leer, Vertikal, leer, leer, leer, CelsiusB, leer);
		System.out.printf( "%s%-3s%s%s%s %s %s %s %s %s %s %s %s %s %s %s %s%4s\n" ,     Plus, FahrenheitC, leer, leer, leer, leer, leer, leer, leer, leer, leer, leer, Vertikal, leer, leer, leer, CelsiusC, leer);
		System.out.printf( "%s%-3s%s%s%s %s %s %s %s %s %s %s %s %s %s %s %s %s%4s\n" ,     Plus, FahrenheitD, leer, leer, leer, leer, leer, leer, leer, leer, leer, leer, Vertikal, leer, leer, leer, leer, CelsiusD, leer);
		System.out.printf( "%s%-3s%s%s%s %s %s %s %s %s %s %s %s %s %s %s %s %s%4s\n" ,     Plus, FahrenheitE, leer, leer, leer, leer, leer, leer, leer, leer, leer, leer, Vertikal, leer, leer, leer, leer, CelsiusE, leer);
		
		
		
	}
}
