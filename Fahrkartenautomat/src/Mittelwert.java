import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {
	   
	   Scanner scan = new Scanner(System.in);

	   // (E) "Eingabe"
	   // Werte f�r x und y festlegen:
	   // ===========================
	   System.out.println("Wie viele Werte wollen Sie eingeben?: ");
	   int anzahlWerte = scan.nextInt();
	   double akkumulator = 0;
	   double[] zahlen = new double[anzahlWerte];
	  
	   for(int i = 0; i < anzahlWerte; i++) {
		   System.out.println("Bitte geben Sie einen Wert ein!: ");
		   zahlen[i] = scan.nextDouble();
		   akkumulator += zahlen[i];
	   }
      
	   double m;
	  
	   // (V) Verarbeitung
	   // Mittelwert von x und y berechnen: 
	   // ================================
	   m = akkumulator / anzahlWerte;
	   
	   // (A) Ausgabe
	   // Ergebnis auf der Konsole ausgeben:
	   // =================================
	   System.out.printf("Der Mittelwert ist %.2f\n", m);
	   for (int i = 0; i < zahlen.length; i++) {
		   System.out.print(zahlen[i] + " ,");
	   }
   	}
   
}
