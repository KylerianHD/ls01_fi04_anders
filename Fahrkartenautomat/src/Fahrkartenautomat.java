﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	
    	double wirklichzuZahlenderBetrag;
		double wirklicherrückgabebetrag;
		
		while(true) {
		wirklichzuZahlenderBetrag = fahrkartenbestellungErfassen();
		wirklicherrückgabebetrag = fahrkartenBezahlen(wirklichzuZahlenderBetrag);
		rueckgeldAusgeben(wirklicherrückgabebetrag);
		}
      
    }
    

    public static double Fahrkartenmenue() {
    	
    	Scanner tastatur = new Scanner(System.in);
    	double wirklichzuZahlenderBetrag;
    	System.out.println("Wählen Sie Ihre gewünschte Fahrkarte aus!: ");
    	System.out.println("Einzelfahrschein (1)");
    	System.out.println("Tageskarte (2)");
    	System.out.println("Gruppenkarte (3)");
    	int nutzerWahl = tastatur.nextInt();
    	switch(nutzerWahl) {
    	case 1: wirklichzuZahlenderBetrag = 3.6;
    		break;
    	case 2: wirklichzuZahlenderBetrag = 7.2;
    		break;
    	case 3: wirklichzuZahlenderBetrag = 22.0;
    		break;
    	default: System.out.println("Sie haben eine Fehleingabe getätigt! Es wird der Standartwert: Einzelfahrschein gewählt!");
    		wirklichzuZahlenderBetrag = 3.6;
    	}
    	
    	return wirklichzuZahlenderBetrag;
    }
     
    public static double fahrkartenbestellungErfassen() {
    	double wirklichzuZahlenderBetrag; 
        int wirklichanzahlTickets;
    	Scanner tastatur = new Scanner(System.in);
    	
        wirklichzuZahlenderBetrag = Fahrkartenmenue();        
        int wirklichanzahlTicketsPrüfer = 0;
        while(wirklichanzahlTicketsPrüfer == 0) {
        	
        	System.out.println("Wie viele Tickets wollen Sie kaufen?: ");
            wirklichanzahlTickets = tastatur.nextInt();
        	
        	if(wirklichanzahlTickets <= 10 & wirklichanzahlTickets >= 1) {
            	
        		wirklichzuZahlenderBetrag = wirklichanzahlTickets * wirklichzuZahlenderBetrag;
            	wirklichanzahlTicketsPrüfer = 1;
            	return wirklichzuZahlenderBetrag;
            	
            } else if(wirklichanzahlTickets < 1) {
            	
            	System.out.println("Ungültige Zahl! Bitte nochmal versuchen!/n");
            	System.out.println("Es gelten nur Zahlen zwischen 1 und 10 (1 und 10 mit inbegriffen)!");
            	wirklichanzahlTicketsPrüfer = 0;
            	
            } else if(wirklichanzahlTickets > 10) {
            	
            	wirklichanzahlTicketsPrüfer = 0;
            	System.out.println("Ungültige Zahl! Bitte nochmal versuchen!/n");
            	System.out.println("Es gelten nur Zahlen zwischen 1 und 10 (1 und 10 mit inbegriffen)!");
            	
            }
        }
        
        return wirklichzuZahlenderBetrag;
        
        
    }
    
    public static double fahrkartenBezahlen(double wirklichzuZahlenderBetrag) {
    	double wirklicheingezahlterGesamtbetrag;
		double wirklicheingeworfeneMünze;
		double wirklicherrückgabebetrag;
		
		Scanner tastatur = new Scanner(System.in);
		
		wirklicheingezahlterGesamtbetrag = 0.0;
		while (wirklicheingezahlterGesamtbetrag < wirklichzuZahlenderBetrag) {
			double zwischenergebnis = wirklichzuZahlenderBetrag - wirklicheingezahlterGesamtbetrag;
			System.out.printf("Noch zu zahlen: %.2f Euro\n", zwischenergebnis);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			wirklicheingeworfeneMünze = tastatur.nextDouble();
			wirklicheingezahlterGesamtbetrag += wirklicheingeworfeneMünze;
		}
        
    	fahrkartenAusgeben();
    	
    	wirklicherrückgabebetrag = wirklicheingezahlterGesamtbetrag - wirklichzuZahlenderBetrag;
		return wirklicherrückgabebetrag;
    }
    
    public static void  fahrkartenAusgeben() {
    	
    	System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
		
    }
    
    public static void rueckgeldAusgeben(double wirklicherrückgabebetrag) {
    	
    	if (wirklicherrückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + wirklicherrückgabebetrag + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (wirklicherrückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				wirklicherrückgabebetrag -= 2.0;
			}
			while (wirklicherrückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				wirklicherrückgabebetrag -= 1.0;
			}
			while (wirklicherrückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				wirklicherrückgabebetrag -= 0.5;
			}
			while (wirklicherrückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				wirklicherrückgabebetrag -= 0.2;
			}
			while (wirklicherrückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				wirklicherrückgabebetrag -= 0.1;
			}
			while (wirklicherrückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				wirklicherrückgabebetrag -= 0.05;
			}
		}
    	System.out.println("\nVergessen Sie nicht, den Fahrschein" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.\n\n\n\n");
    	
    	try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        }


}

